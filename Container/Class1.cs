﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container
{
    public class Class1 : IContainer
    {

        public void Register(System.Reflection.Assembly assembly)
        {
            throw new NotImplementedException();
        }

        public void Register(Type type)
        {
            throw new NotImplementedException();
        }

        public void Register<T>(T impl) where T : class
        {
            throw new NotImplementedException();
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            throw new NotImplementedException();
        }

        public T Resolve<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public object Resolve(Type type)
        {
            throw new NotImplementedException();
        }
    }
}
