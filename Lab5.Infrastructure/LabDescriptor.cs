﻿using System;
using System.Reflection;
using Container;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container.Class1);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(void));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(void));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(void));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(void));

        #endregion
    }
}
